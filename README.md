# Telegraf Docker Image with Synology MIBs
---
Telegraf with Synology MIBs from `https://global.download.synology.com/download/Document/MIBGuide/Synology_DiskStation_MIB_Guide.pdf` in `/usr/share/snmp/mibs/`.

Based on https://hub.docker.com/r/nuntz/telegraf-snmp
