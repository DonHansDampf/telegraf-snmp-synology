FROM bushari/telegraf-snmp

RUN apt-get update
RUN apt-get -y install wget p7zip-full
RUN wget -P /tmp "https://global.download.synology.com/download/Document/MIBGuide/Synology_MIB_File.zip"
RUN 7za e -o"/usr/share/snmp/mibs/" /tmp/Synology_MIB_File.zip
# RUN unzip /tmp/Synology_MIB_File.zip -d /usr/share/snmp/mibs/
